# Suitable battery for raspberry pi & State of charge (SOC) finding method

 There are various type of batteries available but while choosing battery for a desired work there are many things to be noted. Let us see those parameter and suitable battery & to find the charge left in it for that I have compared two majorly used battery .
 1. lead acid 
 1. lithium ion

 Before comparing there are some basic parameter to be know about battery
 ### State of Charge(SOC)
 The state of charge (SOC) is a calculation of the amount of energy available in a battery, expressed as a percentage of the energy available in a battery at a given point in time.
 
 SOC=

 example :if SOC is 100% ,then the battery is full. if SOC is 0% ,then it is empty.
 ### Depth of Discharge(DOD)
 Depth of Discharge is the fraction or percentage of the capacity which has been removed from the fully charged battery. It is an alternative method to indicate a battery's State of Charge.

 | parameter | lead acid | lithium ion |
 |-----------|-----------|-------------|
 | cost|low|high|
 |efficiency|low when compared with lithium ion|high|
 |DOD|low|high|
 |size|larger than lithium ion|smaller when compared to lead acid|
 |capacity|high capacity battery are (150Ah)commercially availalbe|less capacity(max 5200mAh)(commerical)|

 By seeing the above table that lithium ion has more advantage than lead acid but the main problem is finding SOC in lithium ion battery is bit difficult.Now let us see why it is difficult and how to is calculated.

There are many methods to find SOC of the battery but now we are going to see only two popular & widely used methods:

## OPEN CIRCUIT VOLTAGE METHOD
It is the simplest method of all.By measuring the terminal voltage og the battery and matching SOC level accordingly.let us see in separate battery

![Gitlab pointers](Battery/lead_acid_vs_li-ion.png)

### Lead acid battery
The terminal voltage and SOC are linearly proportional,So it is easy to find the Terminal voltage ,then we can know appropriate SOC.As the name says it is open circuit voltage,So it is meant for stand still condition(no load),but when connect with load then internal resistance should taken into account.And the result range will be small since the terminal voltage various between(11-13.5)V.

![Gitlab pointers](Battery/lead_acid_curve.png)
 
 ### Lithium ion battery
 Unlike the lead-acid battery, the Li-ion battery does not have a linear relationship between the OCV and SOC. A typical relationship of Li-ion battery between SOC and OCV is shown below:

 ![Gitlab pointers](Battery/Li-ion_Discharge_Voltage_Curve_Typical.jpg)

 This method leads to inaccurate results  for lithium ion battery

 ## COULOMB COUNTING MEHTOD

 In this method the incoming/out going current is used to calculate the SOC value.This will sute for both lead acid & lithium ion batteries.The formula for this methods is :
![Gitlab pointers](Battery/SOc.jpg)

Like other methods it also has some flaws due internal resistance.But it will give accurate result than OCV.Now-a-days there are ready-made IC avaialbe used to calculate SOC like LTC2943-1,LTC2941-1 ect

#### choosen:
Lead acid battery & OCV method can be choosen if it should cost effective 

lithium ion battery can be choosen for long life

