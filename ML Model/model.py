import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_predict

data = pd.read_csv("parameters.csv")  #transformer parameters

#print(data)
data.features = data[["voltage","ambient_temperature","hotspot_temperature","unix_timestamp"]]
data.targets = data.anomaly 

feature_train, feature_test, target_train, target_test = train_test_split(data.features, data.targets, test_size=.2)

model = DecisionTreeClassifier(criterion='gini')
model.fitted = model.fit(feature_train, target_train)
model.predictions = model.fitted.predict(feature_test)

print(confusion_matrix(target_test, model.predictions))
print(accuracy_score(target_test, model.predictions))

predicted = cross_val_predict(model,data.features,data.targets, cv=10)
print(accuracy_score(data.targets,predicted))
