/** @file mqtt.cxx
 *  @brief This program publish the message using mqtt protocol
 *  Here, a JSON configuration file(config.json), with necessary configuration for mqtt connection is set up and published the messages
 *  using Shunya API
 *  @Jaissri
 */

#include <iostream>
#include <fstream>
#include <string>
#include <shunyaInterfaces.h>   /*library which gives access to shunya APIs*/

using namespace std;
/** 
 *  @brief parseJSON function parses the json file and returns the value for given key.
 *  First new object created with parameters of config.json file
 *  Then using Shunya API published the meassages using Shunya MQTT API
 *  @return 0
 */

void send(void){
    mqttObj broker1 = newMqtt("mqtt");  //create a new mqtt object with configurations
    mqttConnect(&broker1) ;             //connect to broker 
    mqttPublish(&broker1, "data","Voltage: %d\nBattery: %d\nAmbient_Temperature: %d\nHotspot_Temperature: %d\nTimestamp: %ld\n", voltage,battery,temp1,temp2,unix_timestamp); //publish following message
    mqttDisconnect(&broker1) ;    //disconnect from broker
    return 0;
}
/*
*  main() function calls the send() function to run and execute code
*/

int main(void){
    initLib();
    int voltage, battery, temp1, temp2;
    long int unix_timestamp;
    send(); //calls the function
    return 0;
}

