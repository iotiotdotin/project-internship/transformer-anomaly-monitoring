***STUDY OF VOLTAGE SENSORS***


Transformer is an electrostatic device which transfers    power from one electrical circuit to another electrical circuit without changing its frequency.
The voltage in transmission and distribution is very high like: 11KV, 33KV, 66KV etc.
So we cannot measure these voltages just by using voltmeter. So we use potential transformer for this.
PT steps down the voltage in a specific ratio and then we can measure that voltage using voltmeter.
There are main two types of voltage sensors which works on voltage divider rule:
1.	capacitive type voltage sensor
2.	resistive type voltage sensor




We can measure voltage using two types of meters/sensors-

1. **ANALOG METER**
The meters which comes under this are:
ELECTROSTATIC VOLTMETER:-
It works on electrostatic principle.
Its voltage rating is nearly about 200kv.
It has high sensitivity.
It is capable of measuring smallest charge voltages.

*ZMPT101B*:-

1.It is a high precision voltage transformer.  
2.Its voltage rating is up to 1kv.      
3.It is of low cost.   
4.It can be easily mounted on PCB.    
5.It has good consistency.



2. **DIGITAL VOLTMETERS:**
The meter which comes under this is: 
 
*149-10A DIGITAL VOLTMETER*:-

1.It can measure high voltages up to 10kv.  
2.It has high accuracy.  
3.It has input impedance.

*ES7 DIGITAL VOLTMETER*:-

1.We can use it in 3 phase system.   
2.We can set the PT ratio freely.


