/**@file sensor_reading.c
   @brief this file read the analog vltage from the sensor
   @author manisha kumari
**/

/* header files */
#include <stdio.h>
#include <shunyaInterfaces.h>   /*library which gives access to shunya APIs*/

/*main function*/
int main(void)
{

/*initilize the library*/
ititLib();

/*read data from sensor*/
float voltage = getAdc(1);  //voltage of transformer
float volt_read1 = (analogRead(GPIO_PIN_1)/1023)*330;
float temp1 = (volt_read1*1.8)+32;  //ambient temperature
float volt_read2 = (analogRead(GPIO_PIN_2)/1023)*330;
float temp2 = (volt_read2*1.8)+32;  //hotspot temperature

/*returns 0 if program runs successfully*/
return 0;
}
//submit your code with comments and spacings here
