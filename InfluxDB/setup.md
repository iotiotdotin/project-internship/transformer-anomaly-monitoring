The system provides internal default settings for setting each configuration file. Using the command shown below, you can view and edit the settings in the nano editor.

> nano /etc/influxdb/influxdb.conf  

Most of the settings in the local configuration file /etc/influxdb/influxdb.conf are commented out. All commented settings are determined by the internal default settings. All uncommented settings in the local configuration file override the internal defaults. Note that the local configuration file does not have to already contain all configuration settings.

Before you can work with InfluxDB, e.g. to create, delete or edit (a) database(s) or to analyze data, you have to start InfluxDB once with your configuration file.
There are two possibilities:

1. point the process to the correct configuration file with the -config option.


> influxd -config /etc/influxdb/influxdb.conf  
>8888888 .d888 888 8888888b. 888888b.  
>888 d88P" 888 888 "Y88b 888 "88b  
>888 888 888 888 888 888 .88P  
>888 88888b. 888888 888 888 888 888 888 888 888 8888888K.  
>888 888 "88b 888 888 888 888 Y8bd8P' 888 888 888 "Y88b  
>888 888 888 888 888 888 888 X88K 888 888 888 888  
>888 888 888 888 888 Y88b 888 .d8""8b. 888 .d88P 888 d88P  
>8888888 888 888 888 888 "Y88888 888 888 8888888P" 8888888P"  
>2018-04-04T23:03:51.641110Z info InfluxDB starting {"log_id": "07GjHMMG000", "version": "1.5.1", "branch": "1.5", "commit": "cdae4ccde4c67c3390d8ae8a1a06bd3b4cdce5c5"}  
>2018-04-04T23:03:51.641142Z info Go runtime {"log_id": "07GjHMMG000", "version": "go1.9.2", "maxprocs": 4}  
>run: open server: listen: listen tcp 127.0.0.1:8088: bind: address already in use  

2. Set the environment variable INFLUXDB_CONFIG_PATH in relation to the path of your configuration file and start the process.  


> echo $INFLUXDB_CONFIG_PATH /etc/influxdb/influxdb.conf  
> influxd  
>8888888 .d888 888 8888888b. 888888b.  
>888 d88P" 888 888 "Y88b 888 "88b  
>888 888 888 888 888 888 .88P  
>888 88888b. 888888 888 888 888 888 888 888 888 8888888K.  
>888 888 "88b 888 888 888 888 Y8bd8P' 888 888 888 "Y88b  
>888 888 888 888 888 888 888 X88K 888 888 888 888  
>888 888 888 888 888 Y88b 888 .d8""8b. 888 .d88P 888 d88P  
>8888888 888 888 888 888 "Y88888 888 888 8888888P" 8888888P"  
>2018-04-04T23:43:31.184743Z info InfluxDB starting {"log_id": "07GlYaS0000", "version": "1.5.1", "branch": "1.5", "commit": "cdae4ccde4c67c3390d8ae8a1a06bd3b4cdce5c5"}  
>2018-04-04T23:43:31.184777Z info Go runtime {"log_id": "07GlYaS0000", "version": "go1.9.2", "maxprocs": 4}  
>run: open server: listen: listen tcp 127.0.0.1:8088: bind: address already in use    

InfluxDB works by first checking the configuration using the -config option and then checking the configuration using the environment variable.
Furthermore, you must make sure that the directories where data and the Write Ahead protocol (WAL) are stored are writable for you before running the InfluxDB service. In other words, you need to check if these directories exist. In case the data and WAL directories are not writable, the InfluxdDB service is not started. Both should be in the /var/lib/influxdb directory. You can display the directories in the terminal with the following two commands and thus guarantee their writability.


> cd /var/lib/influxdb  
> ls  