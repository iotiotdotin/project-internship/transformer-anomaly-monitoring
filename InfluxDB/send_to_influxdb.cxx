/* --- Standard Includes --- */
#include <iostream>
#include <fstream>
#include <string>

/* --- Includes for using rapidJSON library to parse JSON --- */
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <rapidjson/ostreamwrapper.h>

/* --- Includes ShunyaOS Interface---*/
#include <shunyaInterfaces.h>

using namespace rapidjson;
using namespace std;


void send_data(int voltage, long int unix_timestamp,int temp1, int temp2, int battery)
{
	influxdbObj influxdb = newInfluxdb("test-db");
	writeDbInflux (influxdb, "measurement, voltage=%d,battery=%d,ambient_temperature=%d,hotspot_temperature=%d, %ld",voltage,battery,temp1,temp2,unix_timestamp);
}

int main(void)
{
	initLib();
	int battery,voltage;
	long int unix_timestamp;
	send_data(voltage, unix_timestamp, temp1, temp2, battery);
 /* the data (i.e. voltage,ambient temperature, hotspot temperature and UNIX timestamp) would be got from the JSON file (i.e. after conversion of raw data into JSON format). */
	return(0);
}
