## Steps to Install InfluxDb  
Installation of the InfluxDB package may require root or administrator privileges in order to complete successfully.
Ubuntu users can install the latest stable version of InfluxDB using the apt-get package manager.

#### Add the InfluxData repository with the following commands:
>curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -  
>source /etc/lsb-release  
>echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
#### Then, install and start the InfluxDB service:  
>sudo apt-get update && sudo apt-get install influxdb  
>sudo service influxdb start  
#### Or if your operating system is using systemd (Ubuntu 15.04+, Debian 8+):  
>sudo apt-get update && sudo apt-get install influxdb  
>sudo systemctl unmask influxdb.service  
>sudo systemctl start influxdb  
#### There are two ways to launch InfluxDB with your configuration file:
* Point the process to the correct configuration file by using the -config option:  
>influxd -config /etc/influxdb/influxdb.conf  
* Set the environment variable INFLUXDB_CONFIG_PATH to the path of your configuration file and start the process. For example:  
>echo $INFLUXDB_CONFIG_PATH  
>/etc/influxdb/influxdb.conf  
>influxd  
InfluxDB first checks for the -config option and then for the environment variable.  

#### Running the container
The InfluxDB image exposes a shared volume under /var/lib/influxdb, so you can mount a host directory to that point to access persisted container data. A typical invocation of the container might be:  

>$ docker run -p 8086:8086 \  
>      -v $PWD:/var/lib/influxdb \  
>      influxdb  

Modify $PWD to the directory where you want to store data associated with the InfluxDB container. You can also have Docker control the volume mountpoint by using a named volume.

>$ docker run -p 8086:8086 \
>      -v influxdb:/var/lib/influxdb \
>      influxdb