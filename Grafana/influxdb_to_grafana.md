## Adding Data Source as InfluxDB
***
1. Go to *configuration bar* and click on **Data Source**.
2. Choose **InfluxDB** as your *data source*.
3. Now add the following details.
    * Query Language : InfluxQL
    * HTTP url: http://localhost:8086
    * Database : *your_influxDB_database_name*
    * HTTP Method: Get
4. You can see the button below **Save and Test**. Click it.
5. Now you can see your data source is added as InfluxDB.
6. It's the connection set-up.