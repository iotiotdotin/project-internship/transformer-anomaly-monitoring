## Grafana
***

1. Grafana is an open source visualization and analytics software.
2. It allows you to query, visualize, alert on, and explore your metrics no matter where they are stored.
3. It provides you with tools to turn your time-series database (TSDB) data into beautiful graphs and visualizations.

    ![Grafana Lab Overview of a dashboard](https://miro.medium.com/max/1000/1*KimwgjULRZzONpjGFH1sTA.png)

### Features of Grafana Open Source Software
***

1. **Explore Metrics and Logs :** Here we can explore the data through ad-hoc queries and dynamic drilldown. Also we can split view and compare different time ranges, queries and data sources side by side.
2. **Alerts :** Using Grafana alerting, we can have alerts sent through a number of different alert notifiers; including PagerDuty, SMS, email, VictorOps, OpsGenie, or Slack.
3. **Annotations :** This feature, which shows up as a graph marker in Grafana, is useful for correlating data in case something goes wrong. In this we can *Annotate* graphs, with rich events from different data sources. Hover over events to see the full event metadata and tags.
4. **Dashboard Variables :** *Template variables* allow you to create dashboards that can be reused for lots of different use cases. We can also share these dashboards across teams within the organization or if someone creates a great dashboard template for a popular data source, he can contribute it to the whole community to customize and use.
5. **Configure Grafana :** Configuration covers both config files and environment variables. We can set up default ports, logging levels, email IP addresses, security, and more.
6. **Authentication :** Grafana supports different authentication methods, such as LDAP and OAuth, and allows you to map users to organizations.
7. **Provisioning :** While it’s easy to click, drag, and drop to create a single dashboard, power users in need of many dashboards will want to automate the setup with a script. We can script anything in Grafana.
8. **Grafana Cloud :** Grafana Cloud is a highly available, fast, fully managed OpenSaaS logging and metrics platform. Everything you love about Grafana, but Grafana Labs hosts it for you and handles all the headaches.

### Explore Grafana 
***

1. Grafana is an in-built Dashboard for every type of representation you want to show.
2. Follow the installation process first.  [Click Here](/Grafana/grafana_setup.md)
3. **Now create your own dashboard roughly and explore its features.**

### Using InfluxDB with Grafana
***

#### Basics to know
***

1. Grafana has a tight integration with InfluxDB.
2. InfluxDB has an API, that defaults to port 8086; while Grafana’s API is on port 3000.
3. Grafana will call the InfluxDB API, whenever it wants to query data.

    **Note: To install and set-up Grafana properly follow the steps in the link [Grafana Setup](/Grafana/grafana_setup.md)**


