## Stepwise procedure to set-up Grafana Dashboard
***
#### Primary Setup (Installation and set-up)
***
1. First of all install Grafana and run.  
    Using Docker (for an Ubuntu base image):

        docker run -d --name=grafana -p 3000:3000 grafana/grafana:7.2.0-beta2-ubuntu

2. Now, open your web browser and open **http://localhost:3000**.
3. You are able to see the **login** bar of Grafana.
4. In the *Email or Username* field, type **admin** and also in the *password* field type **admin**.
5. Now, click on login button and *change your password* into a strong one.
6. Now, Grafana is ready to work, you are in its home page.

#### Adding Data Source
***
1. Go to *configuration bar* and click on **Data Source**.
2. Choose **InfluxDB** as your *data source*.
3. Now add the following details.
    * Query Language : InfluxQL
    * HTTP url: http://localhost:8086
    * Database : *your_influxDB_database_name*
    * HTTP Method: Get
4. You can see the button below **Save and Test**. Click it.
5. Now you can see your data source is added as InfluxDB and working as expected.

#### Creating Dashboard
***
1. Click on **Create New Dashboard**.
2. Click on **Add New Panel**.
3. Add the Query Details:
    * Write the measurement name, with conditions of where clause.
    * In the select section add the field name which you want to show in your pictorial representation.
    * You can add group by section as needed for you.
    * Now add the **alias** to make it clear to understand the tag name.
4. Refer the following image : 

    ![Query Details](/Grafana/images/query_details.jpg)

5. Now add the **Panel title** at the right side bar **Settings**.
6. Add the time range in which you want to get the data to display.
6. Now click on **Save** button at the top right cornor.
7. Add the **Dashboard name** and click *save*.
8. In this process create three panels with different visualizations as needed (for *battery percentage* and *voltage* you can use visualization **Gauge** and for *voltage waveform* you can use **Graph**).
9. You can add one more panel for alert messages using the visualization **Alert list**.
10. Your Grafana Dashboard is ready to be used.
11. Here is a preview of your dashboard :

    ![Dashboard preview](/Grafana/images/dashboard_preview.jpg)

12. In the above figure the **no data** would be replaced by a beautiful visualization for your actual database once you add the **data source** and **query details**.

### Alerting 
***
1. In Grafana we can set some alerting rules to alert, when necessary.
2. To do that, during the panel setting up, click on alert (you can see **alert** is written aside in the image above).
3. Click on **Add Alert**.
4. Now add the alert rule you want.

***

## 